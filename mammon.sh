#!/usr/bin/env bash

# Mammon's compass
# Get data from www.kununu.com into a csv file (CH, DE, AT)

# Get the links from 
# https://www.kununu.com/de/search#/?country=COUNTRY_CH
# pages 1 to N.
# ToDo; Get "N" or choose a reasonable value.  
for i in `seq 1 2`;
do
  echo "Downloading page number:" $i
  phantomjs save_page.js "https://www.kununu.com/de/search#/?country=COUNTRY_CH&page=$i" > ./page-search-$i.html 
  # save_page.js comes from [[https://superuser.com/questions/448514/command-line-browser-with-js-support]]

  # The lines of interest have the form: 
  #  <a href="/ch/swisscom" class="ng-binding">Swisscom AG</a>
  # Get the data in json format.
  # ToDo: An HTML-savvy developer should look at this.  
  cat page-search-$i.html | pup '.panel-mast a[href] json{}' > data-p1-$i.html

  cat data-p1-$i.html | grep -i href > tmp-p01-$i.txt  # We get the urls.
  # cat test-json.html | grep -i title  # We get the company name. 


  # Edit the file to make it a proper list of urls. 
  awk '{print $2}' tmp-p01-$i.txt > tmp-$i.txt 
  sed -i -e "s/\"//g" tmp-$i.txt
  sed -i -e "s/,//g" tmp-$i.txt
  # Adding the "https://www.kununu.com" to the start of each line. 
  sed -e 's/^/https\:\/\/www.kununu.com/' tmp-$i.txt > urls-q01-$i.txt
  # With this we have the urls of all companies of page "i". 

done    

cat urls-q01-* > kununu-all-company-urls.txt
rm urls-q01-*.txt tmp-*.txt page-search-*.html data-p1-*.html
echo "" >> kununu-all-company-urls.txt  # Add newline at the end.
# ToDo; Only add newline if last line is not empty. 

# Get info from each company url in the file.
while read p; do
  echo "$p"
  shortname=$(echo "$p" | sed 's/.*\///')
  echo "$shortname"
  echo "--- --- --- --- --- --- --- --- --- "

  phantomjs save_page.js $p > company-$shortname.html
  
  # BUG: "pup 'script:nth-of-type(18)'" only works sometimes.
  # A workaround would be to add a check for the correct output,
  # but it's a HACK. 
  # NOTE: Keep this separated, so we can easily check for "totalReviews".
  # That indicates the output is correct.
  
  # Extract segment of interest. 
  cat company-$shortname.html | pup 'script:nth-of-type(18)' > company-data-$shortname.txt

  # Clean it.
  cat company-data-$shortname.txt | grep -v "script\>" | \
  grep -v "window.dataLayer" | grep -v "pageType" | \
  grep -v "});" > company-data-$shortname-tmp.txt

  # Remove indentation.
  awk '{$1=$1};1' company-data-$shortname-tmp.txt > company-data-$shortname.txt

  # Clean it further.
  sed -i.bak -e "s/.*\: //g" company-data-$shortname.txt 

  # NOTE: We remove the "benefits" item for now, as it's troublesome. 
  # ToDo: Include the "benefits" field. 
  cat company-data-$shortname.txt | grep -v JSON > company-data-$shortname-tmp.txt
  mv company-data-$shortname-tmp.txt company-data-$shortname.txt

  sed -i.bak -e "s/',//g" company-data-$shortname.txt
  sed -i.bak -e "s/'//g" company-data-$shortname.txt

  # Join into a single line to turn it into CSV, with ";" as separator.
  cat company-data-$shortname.txt | tr "\n" ';' > company-data-$shortname.csv
  echo "" >> company-data-$shortname.csv  # Add newline at the end.

  cat company-data-$shortname.csv >> kununu-switzerland.csv

  echo "$shortname data"
  cat company-data-$shortname.csv  # BUG: Bug will be visible here (as empty).

  rm company-data-$shortname.txt company-data-$shortname.csv company-data-$shortname.txt.bak

done <kununu-all-company-urls.txt

echo "" >> kununu-switzerland.csv  # Add newline at the end.

mkdir company-pages
mv company-*.html ./company-pages/
